
import { combineReducers, createStore } from 'redux';

import counterReducer, { CounterState, initialState as initialCounter } from './counter/counterSlice';

export interface RootState {
    counter: CounterState;
}

const initState: RootState = {
    counter: initialCounter,
}

const reducer = combineReducers({
    counter: counterReducer,
});

export const store = createStore(
    reducer,
    initState
);

export default store;

