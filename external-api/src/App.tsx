import './App.css';

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import logo from './logo.svg';
import { increment, selectCounterValue } from './state/counter/counterSlice';

// random user API
// https://randomuser.me/

// guess nationality
// https://api.nationalize.io/?name=X

function App() {
  const dispatch = useDispatch();
  const counterValue = useSelector(selectCounterValue)

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <span>Counter value: { counterValue }</span>
        <button onClick={() => dispatch(increment())}>Increment counter</button>
      </header>
    </div>
  );
}

export default App;
